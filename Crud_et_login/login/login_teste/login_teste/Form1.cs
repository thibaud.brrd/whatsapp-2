﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace login_teste
{
    public partial class frmLogin : Form
    {
        function db = new function();
        string password;
        string username;
        string result;

        public frmLogin()
        {
            InitializeComponent();
            refresh();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            ValidateEntre();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void btnAffiche_Click(object sender, EventArgs e)
        {
            lsbUsers.DataSource = db.Select()[1];
            lsbAge.DataSource = db.Select()[2];
        }

        /// <summary>
        /// Affiche les données
        /// </summary>
        public void refresh() {
            password    = "";
            username    = "";
            result      = "";
            tbxUsername.Text = username;
            tbxAge.Text      = password;
            lblResult.Text   = result;
        }

        /// <summary>
        /// Vérification des champs 
        /// </summary>
        public void ValidateEntre()
        {
            // première condition : vérifie si le name existe  || deuximée condtion : vérifie si le mot de passe et le meme que dans la db
          if (db.Select()[1].Contains(tbxUsername.Text) && tbxAge.Text == db.lireUtilisateur(tbxUsername.Text).age.ToString())
          {
              lblResult.Text = "successful connection";
          }
          else
          {
              lblResult.Text = "password or username incorect";
          }
            
        }


    }

}
