﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace crud
{
    public partial class frmCrud : Form
    {
        // appelle de la classe avec les fonctions sql
        MySQLConnect db = new MySQLConnect();
        // init variable
        string oldname;
        public frmCrud()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // insert
            db.Insert(tbxInputCmd.Text,Convert.ToInt32(tbxAge.Text));
            // récupérer le nom acctuelle pour faire une modification
            oldname = tbxInputCmd.Text;
            // afficher
            lsbResult.Items.Add(tbxInputCmd.Text + " / " + tbxAge.Text);
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            db.Update(oldname, tbxInputCmd.Text, Convert.ToInt32(tbxAge.Text));
            oldname = tbxInputCmd.Text;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            db.Delete(tbxInputCmd.Text);
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            // select arevoir
           // lsbResult.Items.Add(db.Select());
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            lsbResult.DataSource = db.Select()[1];

        }

        private void btnDelet_Click(object sender, EventArgs e)
        {

            
        }
    }
}
