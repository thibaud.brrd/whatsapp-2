﻿namespace WF_CFPTApp
{
    partial class FrmSelectAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pibLogo = new System.Windows.Forms.PictureBox();
            this.msAccount = new System.Windows.Forms.MenuStrip();
            this.tsmCompte = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDiscusions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.lblTitre = new System.Windows.Forms.Label();
            this.flpUserList = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSignIn = new System.Windows.Forms.Button();
            this.btnSignUp = new System.Windows.Forms.Button();
            this.btnConnection = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).BeginInit();
            this.msAccount.SuspendLayout();
            this.SuspendLayout();
            // 
            // pibLogo
            // 
            this.pibLogo.Image = global::WF_CFPTApp.Properties.Resources.logo2;
            this.pibLogo.Location = new System.Drawing.Point(12, 27);
            this.pibLogo.Name = "pibLogo";
            this.pibLogo.Size = new System.Drawing.Size(130, 130);
            this.pibLogo.TabIndex = 1;
            this.pibLogo.TabStop = false;
            // 
            // msAccount
            // 
            this.msAccount.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCompte,
            this.tsmDiscusions,
            this.tsmHelp});
            this.msAccount.Location = new System.Drawing.Point(0, 0);
            this.msAccount.Name = "msAccount";
            this.msAccount.Size = new System.Drawing.Size(1264, 24);
            this.msAccount.TabIndex = 2;
            this.msAccount.Text = "menuStrip1";
            // 
            // tsmCompte
            // 
            this.tsmCompte.Name = "tsmCompte";
            this.tsmCompte.Size = new System.Drawing.Size(62, 20);
            this.tsmCompte.Text = "Compte";
            // 
            // tsmDiscusions
            // 
            this.tsmDiscusions.Name = "tsmDiscusions";
            this.tsmDiscusions.Size = new System.Drawing.Size(75, 20);
            this.tsmDiscusions.Text = "Discusions";
            // 
            // tsmHelp
            // 
            this.tsmHelp.Name = "tsmHelp";
            this.tsmHelp.Size = new System.Drawing.Size(44, 20);
            this.tsmHelp.Text = "Help";
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitre.Location = new System.Drawing.Point(161, 60);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(384, 55);
            this.lblTitre.TabIndex = 10;
            this.lblTitre.Text = "Acount Selection";
            // 
            // flpUserList
            // 
            this.flpUserList.Location = new System.Drawing.Point(12, 183);
            this.flpUserList.Name = "flpUserList";
            this.flpUserList.Size = new System.Drawing.Size(1240, 440);
            this.flpUserList.TabIndex = 11;
            // 
            // btnSignIn
            // 
            this.btnSignIn.Location = new System.Drawing.Point(1036, 137);
            this.btnSignIn.Name = "btnSignIn";
            this.btnSignIn.Size = new System.Drawing.Size(105, 40);
            this.btnSignIn.TabIndex = 12;
            this.btnSignIn.Text = "Sign In";
            this.btnSignIn.UseVisualStyleBackColor = true;
            this.btnSignIn.Click += new System.EventHandler(this.btnSignIn_Click);
            // 
            // btnSignUp
            // 
            this.btnSignUp.Location = new System.Drawing.Point(1147, 137);
            this.btnSignUp.Name = "btnSignUp";
            this.btnSignUp.Size = new System.Drawing.Size(105, 40);
            this.btnSignUp.TabIndex = 13;
            this.btnSignUp.Text = "Sign Up";
            this.btnSignUp.UseVisualStyleBackColor = true;
            this.btnSignUp.Click += new System.EventHandler(this.btnSignUp_Click);
            // 
            // btnConnection
            // 
            this.btnConnection.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnConnection.Location = new System.Drawing.Point(577, 629);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(105, 40);
            this.btnConnection.TabIndex = 15;
            this.btnConnection.Text = "Connexion";
            this.btnConnection.UseVisualStyleBackColor = true;
            // 
            // FrmSelectAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.btnSignUp);
            this.Controls.Add(this.btnSignIn);
            this.Controls.Add(this.flpUserList);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.pibLogo);
            this.Controls.Add(this.msAccount);
            this.MainMenuStrip = this.msAccount;
            this.Name = "FrmSelectAccount";
            this.Text = "CFPTAPP";
            this.Load += new System.EventHandler(this.FrmSelectAccount_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).EndInit();
            this.msAccount.ResumeLayout(false);
            this.msAccount.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pibLogo;
        private System.Windows.Forms.MenuStrip msAccount;
        private System.Windows.Forms.ToolStripMenuItem tsmCompte;
        private System.Windows.Forms.ToolStripMenuItem tsmDiscusions;
        private System.Windows.Forms.ToolStripMenuItem tsmHelp;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.FlowLayoutPanel flpUserList;
        private System.Windows.Forms.Button btnSignIn;
        private System.Windows.Forms.Button btnSignUp;
        private System.Windows.Forms.Button btnConnection;
    }
}