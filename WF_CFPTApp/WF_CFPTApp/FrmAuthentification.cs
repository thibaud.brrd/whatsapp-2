﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_CFPTApp
{
    public partial class FrmAuthentification : Form
    {
        // Constantes
        #region Constantes
        const int CODE_AUTHENTIFICATION = 123987;
        #endregion

        // Champs
        #region Champs

        #endregion

        // Propriétés
        #region Propriétés

        #endregion

        // Constructeurs
        #region Constructeurs
        public FrmAuthentification()
        {
            InitializeComponent();
        }

        #endregion

        // Méthodes
        #region Méthodes
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(tbxCode.Text) == CODE_AUTHENTIFICATION)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                tbxCode.Text = String.Empty;
            }
        }

        #endregion
    }
}
