﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF_CFPTApp
{
    class Message
    {
        public int id { get; set; }
        public string date { get; set; }
        public string senderId { get; set; }
        public string receiverId { get; set; }
        public string content { get; set; }


        public override string ToString()
        {
            return this.id + " : " + this.senderId + " : " + this.receiverId + " : " + this.content + " : " + this.date;
        }

    }
}
