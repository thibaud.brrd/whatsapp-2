﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_cfpta_design
{
    public partial class frmAuthentification : Form
    {
        // Constantes
        #region Constantes
        const int CODE_AUTHENTIFICATION = 123987;
        #endregion

        // Propriétés
        #region Propriétés

        #endregion

        // Constructeurs
        #region Constructeurs
        public frmAuthentification()
        {
            InitializeComponent();
        }
        #endregion

        private void tbxCode_Click(object sender, EventArgs e)
        {
            tbxCode.Clear();

            pbCode.BackgroundImage = Properties.Resources.icons8_code_24_bleu;
            panelUnderLineTbxCode.BackColor = Color.FromArgb(78, 184, 206);
            tbxCode.ForeColor = Color.FromArgb(78, 184, 206);
            lblTextCode.ForeColor = Color.FromArgb(78, 184, 206);
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(tbxCode.Text) == CODE_AUTHENTIFICATION)
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                tbxCode.Text = String.Empty;
            }
        }
    }
}
