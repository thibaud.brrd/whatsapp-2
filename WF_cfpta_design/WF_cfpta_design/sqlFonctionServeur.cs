﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;   //Ajouter la dll pour pouvoir l'utiliser
using System.Windows.Forms;

namespace WF_cfpta_design
{
    class SQLFunctionServeur
    {
        private MySql.Data.MySqlClient.MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public SQLFunctionServeur()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "10.5.42.51";
            database = "cfpttchat";
            uid = "root";	//Renseigner
            password = "";	//Renseigner
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySql.Data.MySqlClient.MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        #region Select statement
        /// <summary>
        /// Selectione tous les données de la table Users
        /// </summary>
        /// <returns> un tableau de liste de string </returns>
        public List<string>[] SelectAllUsers()
        {
            string query = "SELECT * FROM users";

            //Create a list to store the result
            List<string>[] list = new List<string>[2];
            list[0] = new List<string>();
            list[1] = new List<string>();


            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["email"] + "");
                    list[1].Add(dataReader["privateKey"] + "");
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        /// <summary>
        /// Selectione tous les données de la table Users
        /// </summary>
        /// <returns> un tableau de liste de string </returns>
        public List<string>[] SelectAllPersons()
        {
            string query = "SELECT * FROM persons";

            //Create a list to store the result
            List<string>[] list = new List<string>[5];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();
            list[3] = new List<string>();
            list[4] = new List<string>();



            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["email"] + "");
                    list[1].Add(dataReader["username"] + "");
                    list[2].Add(dataReader["imageUrl"] + "");
                    list[3].Add(dataReader["publicKey"] + "");
                    list[4].Add(dataReader["isDeleted"] + "");

                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        /// <summary>
        /// Selectione tous les données de la table Users
        /// </summary>
        /// <returns> un tableau de liste de string </returns>
        public List<string>[] SelectAllMessages()
        {
            string query = "SELECT * FROM messages";

            //Create a list to store the result
            List<string>[] list = new List<string>[5];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();
            list[3] = new List<string>();
            list[4] = new List<string>();



            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["date"] + "");
                    list[2].Add(dataReader["senderId"] + "");
                    list[3].Add(dataReader["receiverId"] + "");
                    list[4].Add(dataReader["content"] + "");

                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }
        #endregion

        // Update
        //public void Update(string oldName, string newName, int newAge)
        //{

        //    string query = "UPDATE users SET name='" + newName + "', age='" + newAge + "' WHERE name='" + oldName + "'";

        //    //Open connection
        //    if (this.OpenConnection() == true)
        //    {
        //        //create mysql command
        //        MySqlCommand cmd = new MySqlCommand();
        //        //Assign the query using CommandText
        //        cmd.CommandText = query;
        //        //Assign the connection using Connection
        //        cmd.Connection = connection;

        //        //Execute query
        //        cmd.ExecuteNonQuery();

        //        //close connection
        //        this.CloseConnection();
        //    }

        //}

        #region Insert

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"> nom de l'utilisateur </param>
        /// <param name="mail"> email de l'utilisateur </param>
        /// <param name="ImageUrl"> image de profile </param>
        /// <param name="publickey"> clé public de l'utilisateur </param>
        /// <param name="isDeleted"> Est-ce que le compte est suprimer ? </param>
        public void InsertNesPersons(string name, string mail, string ImageUrl, string publickey, bool isDeleted)
        {
            string query = "INSERT INTO persons(username, email, imageUrl, publicKey, isDeleted) VALUES('" + name + "', '" + mail + "', '" + ImageUrl + "', '" + publickey + "', '" + isDeleted + "')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Crée un nouveux messages dans la db
        /// </summary>
        /// <param name="date"></param>
        /// <param name="senderId"></param>
        /// <param name="receiverId"></param>
        /// <param name="content"></param>
        public void InsertNesMessages(string date, string senderId, string receiverId, string content)
        {
            string query = "INSERT INTO messages(date, senderId, receiverId, content) VALUES('" + date + "', '" + senderId + "', '" + receiverId + "', '" + content + "')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Les "Users sont les contactes de la personne actuelle".
        /// On y retrouve seulement sa clé priver et son identifiant car c'est la table que  tout le monde voie.
        /// </summary>
        /// <param name="mail"> Identifiant unique </param>
        /// <param name="privateKey"> Clé priver qui sert a la comunication</param>
        public void InsertNesUsers(string mail, string privateKey)
        {
            string query = "INSERT INTO users(email) VALUES('" + mail + "', '" + privateKey + "')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Suprime Un utilisateur en fonction de son mail
        /// </summary>
        /// <param name="email"> Identifiant unique </param>
        public void DeleteUsers(string email)
        {
            string query = "DELETE FROM users WHERE email='" + email + "'";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Suprime Un utilisateur en fonction de son mail
        /// </summary>
        /// <param name="email">Identifiant unique</param>
        public void DeletePersons(string email)
        {
            string query = "DELETE FROM persons WHERE email='" + email + "'";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Suprime un messages de la base de données en fonction du contenu du messages
        /// </summary>
        /// <param name="content">contenu du messages</param>
        public void DeleteMessages(string content)
        {
            string query = "DELETE FROM messages WHERE content='" + content + "'";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        #endregion

        /// <summary>
        /// Permet de récupére les champs qui corespond a un user donné.
        /// c'est classe doit communiquer avec le serveur pour pouvoir récupérer les données (password).
        /// </summary>
        /// <param name="aLire"> identifiant de l'utilisateur </param>
        /// <returns></returns>
        public User lireUtilisateur(string aLire)
        {
            string query = "SELECT email, name, password FROM persons where email = @email";
            User user = null;

            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@email", aLire);

                MySqlDataReader dataReader = cmd.ExecuteReader();

                // 
                while (dataReader.Read())
                {
                    // initialisation des propriétés de la classe avec les données de la db
                    user = new User { username = dataReader.GetString("username"), email = dataReader.GetInt32("email"), password = dataReader.GetInt32("password") };
                }

                //close connection
                this.CloseConnection();

                return user;
            }

            return user;
        }
    }
}

