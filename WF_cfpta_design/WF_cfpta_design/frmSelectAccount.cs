﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_cfpta_design
{
    public partial class frmSelectAccount : Form
    {
        // Constantes
        #region Constantes

        #endregion

        // Champs
        #region Champs
        int _selectedNum;
        SQLFunction _db;
        List<DisplayUser> _listUser;

        #endregion

        // Propriétés
        #region Propriétés
        internal int SelectedNum { get => _selectedNum; set => _selectedNum = value; }

        internal SQLFunction Db { get => _db; set => _db = value; }

        internal List<DisplayUser> ListUser { get => _listUser; set => _listUser = value; }

        #endregion

        // Constructeurs
        #region Constructeurs
        public frmSelectAccount()
        {
            Db = new SQLFunction();
            ListUser = new List<DisplayUser>();

            InitializeComponent();
        }

        #endregion

        // Méthodes
        #region Méthodes
        private void FrmSelectAccount_Load(object sender, EventArgs e)
        {
            btnConnexion.Enabled = false;
            UpdateUserList();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            // Dialogue de connexion
            DialogResult resultSignIn;
            frmSignIn dialogueSignIn = new frmSignIn();
            this.Hide();
            resultSignIn = dialogueSignIn.ShowDialog();
            // Connexion réussi
            if (resultSignIn == DialogResult.OK)
            {
                // Dialogue d'authentification
                DialogResult resultAuthen;
                frmAuthentification dialogueAuthen = new frmAuthentification();
                this.Hide();
                resultAuthen = dialogueAuthen.ShowDialog();
                // Authentification réussi
                if (resultAuthen == DialogResult.OK)
                {
                    // Ajoute a la liste des utilisateurs connecter à se poste
                    // this.flpUserList.Controls.Add(createDynamiqueUser(counter, dialogue.Profile, dialogue.Username));
                }
            }
            // Click sur le bouton inscription de la page connexion
            else if (resultSignIn == DialogResult.Abort)
            {
                // fait appel au dialogue d'inscription
                btnSignUp_Click(sender, e);
            }
            // Retour à la page de selection de compte
            this.Show();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            // Dialogue d'inscription
            DialogResult resultSignUp;
            frmSignUp dialogueSignUp = new frmSignUp();
            this.Hide();
            resultSignUp = dialogueSignUp.ShowDialog();
            // Inscription réussi
            if (resultSignUp == DialogResult.OK)
            {
                // Dialogue d'authentification
                DialogResult resultAuthen;
                frmAuthentification dialogueAuthen = new frmAuthentification();
                this.Hide();
                resultAuthen = dialogueAuthen.ShowDialog();
                // Authentification réussi
                if (resultAuthen == DialogResult.OK)
                {
                    // Ajouter a la DB
                    UpdateUserList();
                }
            }
            // Click sur le bouton connexion de la page inscription
            else if (resultSignUp == DialogResult.Abort)
            {
                // fait appel au dialogue de connexion
                btnSignIn_Click(sender, e);
            }
            // Retour à la page de selection de compte
            this.Show();
        }
        private void ctrUser_Click(object sender, EventArgs e)
        {
            // Initialisation
            #region Initialisation
            string num = "";
            // On récupère le controle (profile) sur lequel on a cliqué
            Control ctr = sender as Control;

            #endregion

            // Traitement
            #region Traitement
            // On sauvegarde son num (se trouve dans le tabindex) en tant que selectionné
            for (int i = 0; i < ctr.Name.Length; i++)
            {
                if (Char.IsDigit(ctr.Name[i]))
                    num += ctr.Name[i];
            }
            SelectedNum = Convert.ToInt32(num) - 1;

            // On passe le bouton de connexion en Enable (un profile à été sélectionner on peut donc se connecter)
            btnConnexion.Enabled = true;

            // Change la couleur du fond pour indiquer quel profil est sélectionné
            foreach (Control user in flpUserList.Controls)
            {
                GroupBox gb = user as GroupBox;
                gb.BackColor = Color.FromArgb(34, 36, 49); ;
            }
            if (!(ctr is GroupBox))
            {
                ctr = ctr.Parent;
            }
            ctr = ctr as GroupBox;
            ctr.BackColor = Color.FromArgb(98, 134, 206);

            #endregion
        }

        private Control createDynamiqueUser(int num, Image img, string name)
        {
            GroupBox gbxUser = new System.Windows.Forms.GroupBox();
            PictureBox pibImageUser = new System.Windows.Forms.PictureBox();
            TextBox tbxNameUser = new System.Windows.Forms.TextBox();

            gbxUser.Controls.Add(tbxNameUser);
            gbxUser.Controls.Add(pibImageUser);
            gbxUser.Location = new System.Drawing.Point(3 + 210 * num, 3);
            gbxUser.Name = "gbxUser" + (num + 1);
            gbxUser.Size = new System.Drawing.Size(200, 200);
            gbxUser.TabIndex = num;
            gbxUser.TabStop = false;
            gbxUser.Text = "User " + (num + 1);
            gbxUser.Click += new System.EventHandler(this.ctrUser_Click);
            gbxUser.BackColor = Color.FromArgb(34, 36, 49);

            pibImageUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pibImageUser.Location = new System.Drawing.Point(50, 25);
            pibImageUser.Name = "pibImageUser" + (num + 1);
            pibImageUser.Size = new System.Drawing.Size(100, 100);
            pibImageUser.TabIndex = 0;
            pibImageUser.TabStop = false;
            pibImageUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            pibImageUser.Image = img;
            pibImageUser.Click += new System.EventHandler(this.ctrUser_Click);

            tbxNameUser.Enabled = false;
            tbxNameUser.Location = new System.Drawing.Point(25, 150);
            tbxNameUser.Name = "tbxNameUser" + (num + 1);
            tbxNameUser.Size = new System.Drawing.Size(150, 20);
            tbxNameUser.TabIndex = 1;
            tbxNameUser.ReadOnly = true;
            tbxNameUser.Text = name;
            tbxNameUser.Click += new System.EventHandler(this.ctrUser_Click);

            return gbxUser;
        }

        public DisplayUser getSelectedUser()
        {
            DisplayUser me;

            me = ListUser[SelectedNum];

            return me;
        }

        public void UpdateUserList()
        {
            // Création d'une pseudo query pour les besoins du test
            List<string>[] query = new List<string>[3];
            query[0] = new List<string>();
            query[1] = new List<string>();
            query[2] = new List<string>();

            query[0].Add("brian.grn@eduge.ch");
            query[0].Add("darius.gmsds@eduge.ch");
            query[0].Add("t@b.c");


            query[1].Add("Brian");
            query[1].Add("Darius");
            query[1].Add("titi");

            //query[2].Add("C:/Users/THIBAUDBRRD/Pictures/31648.png");
            //query[2].Add("C:/Users/THIBAUDBRRD/Pictures/planete.jpg");
            //query[2].Add("C:/Users/THIBAUDBRRD/Pictures/Diapo/loutre019.jpg");
            //List<string>[] query = _db.Select();
            int counter = 0;

            for (int i = 0; i < query[0].Count; i++)
            {
                //listUser.Add(new UserDisplay(query[0][i], query[1][i], Image.FromFile(query[2][i])));
                ListUser.Add(new DisplayUser(query[0][i], query[1][i], WF_cfpta_design.Properties.Resources.test));
            }

            foreach (DisplayUser user in ListUser)
            {
                this.flpUserList.Controls.Add(createDynamiqueUser(counter, user.Profile, user.Username));
                counter++;
            }
        }

        #endregion
    }
}
